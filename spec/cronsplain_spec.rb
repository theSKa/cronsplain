RSpec.describe Cronsplain do
  it "has a version number" do
    expect(Cronsplain::VERSION).not_to be nil
  end

  describe Cronsplain::Field do
    it "parses numeric month" do
      expect(Cronsplain::Field.new(:months, '12').values[0].numeric_values).to eql([12])
    end

    it "parses numeric month range" do
      expect(Cronsplain::Field.new(:months, '1-3').values[0].numeric_values).to eql([1, 2, 3])
    end

    it "parses named month range" do
      expect(Cronsplain::Field.new(:months, 'FEB-APR').values[0].numeric_values).to eql([2, 3, 4])
    end

    it "parses named month range cross modulo" do
      expect(Cronsplain::Field.new(:months, 'NOV-FEB').values[0].numeric_values).to eql([11, 12, 1, 2])
    end

    it "parses numeric month range with step" do
      expect(Cronsplain::Field.new(:hours, '1-8/2').values[0].numeric_values).to eql([1, 3, 5, 7])
    end

    it "parses full month range" do
      expect(Cronsplain::Field.new(:months, '*').values[0].numeric_values).to eql((1..12).to_a)
    end

    it "parses full month range with step" do
      expect(Cronsplain::Field.new(:months, '*/3').values[0].numeric_values).to eql((1..12).step(3).to_a)
    end

    it "parses composite fields" do
      values = Cronsplain::Field.new(:months, '*/3,11').values
      expect(values[0].numeric_values + values[1].numeric_values).to eql([1, 4, 7, 10, 11])
    end

    it "parses singly composed fields" do
      values = Cronsplain::Field.new(:months, '1,7,11').values
      expect(values[0].numeric_values + values[1].numeric_values + values[2].numeric_values).to eql([1, 7, 11])
    end

    it "parses complex composite fields" do
      values = Cronsplain::Field.new(:hours, '3-10/2,13,16-19').values
      expect(values[0].numeric_values + values[1].numeric_values + values[2].numeric_values).to eql([3, 5, 7, 9, 13, 16, 17, 18, 19])
    end

    it "parses hour range with step" do
      expect(Cronsplain::Field.new(:hours, '0-23/2').values[0].numeric_values).to eql([0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22])
    end

    it "parses hour range with step" do
      expect(Cronsplain::Field.new(:hours, '0-23/2').values[0].numeric_values).to eql([0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22])
    end

    it "is invalid when wrong characters" do
      expect(Cronsplain::Field.new(:hours, 'asdf').valid?).to be_falsey
    end

    it "is invalid when wrong characters" do
      expect(Cronsplain::Field.new(:months, 'AWS').valid?).to be_falsey
    end

    it "is invalid when wrong composite characters" do
      expect(Cronsplain::Field.new(:months, 'AWS,2-3').valid?).to be_falsey
    end

  end

  describe Cronsplain::Input do
    let(:expected_lines) {
      [
          "minutes        0 15 30 45",
          "hours          0",
          "days           1 15",
          "months         1 2 3 4 5 6 7 8 9 10 11 12",
          "day of week    1 2 3 4 5",
          "command        /usr/bin/find"
      ]
    }

    context 'with basic CLI appellation' do
      let(:basic_command) {'*/15 0 1,15 * 1-5 /usr/bin/find'.split(/\s/)}
      it "parses all elements correctly" do
        expect(Cronsplain::Input.new(basic_command).parse).to eql(expected_lines)
      end
    end

    context 'with complex CLI appellation' do
      let(:complex_command) {
        "-arguments */15 0 1,15 * 1-5 /usr/bin/find".split(/\s/)
      }
      it "parses all elements correctly" do
        expect(Cronsplain::Input.new(complex_command).parse).to eql(expected_lines)
      end
    end
  end
end

