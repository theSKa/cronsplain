require 'cronsplain/field_value'

module Cronsplain
  class Field
    def initialize(type, argument)
      @type = type
      @raw_argument = argument
    end

    def values
      # return @values if @values
      raw_values.map do |value|
        Cronsplain::FieldValue.new @type, value
      end
    end

    def valid?
      values.all? {|field_value| field_value.valid?}
    end

    private
    def raw_values
      @raw_argument&.split(',') || []
    end
  end
end