module Cronsplain
  class FieldValue

    MAPPINGS = {
        minutes: {
            values: (0..59).to_a,
            names: []
        },
        hours: {
            values: (0..59).to_a,
            names: []
        },
        days: {
            values: (1..31).to_a,
            names: []
        },
        months: {
            values: (1..12).to_a,
            names: %w(JAN FEB MAR APR MAY JUN JUL AUG SEP OCT NOV DEC)
        },
        day_of_week: {
            values: (0..6).to_a,
            names: %w(SUN MON TUE WED THU FRI SAT)
        }
    }.freeze



    def initialize(type, raw_value)
      @type = type
      @raw_value = raw_value
      @normalized_value = named_entry? ? normalize : @raw_value
    end

    def valid?
      all? || numeric_step? || numeric_entry? || named_entry?
    end

    def numeric_values
      return values if all?
      return [@normalized_value.to_i] if numeric_value? || named_value?
      if numeric_range? || named_range?
        range = @normalized_value.split('-').map(&:to_i)
        if range[0] > range[1]
          return Range.new(range[0], values[-1]).to_a + Range.new(values[0], range[1]).to_a
        end
        return Range.new(range[0], range[1]).to_a
      end
      if numeric_step?
        range, jump_step = @normalized_value.split('/')
        range_field = FieldValue.new(@type, range)
        range_values = range_field.numeric_values
        range_values.select.with_index {|v, index| index % jump_step.to_i == 0}
      end
    end

    # We normalize to numeric format always (JAN-FEB -> 1-2)
    def normalize
      replacement = @raw_value.dup
      if named_entry?
        mappings[:names].each_with_index do |name, index|
          replacement = replacement.gsub name, values[index].to_s
        end
      end
      replacement
    end

    private
    # We can optimise even more by mapping these piped values and the regular expressions used only once
    # For now this will suffice as we are not under pressure of optimisation
    def piped_named_values
      return @piped_named_value if @piped_named_value
      @piped_named_value = "(#{names.join('|')})"
    end

    def piped_numeric_values
      return @piped_numeric_value if @piped_numeric_value
      @piped_numeric_value = "(#{values.join('|')})" # "(0|1|2|3|4|5|6)" for day_of_week
    end

    def mappings
      MAPPINGS[@type]
    end

    def values
      mappings[:values]
    end

    def names
      mappings[:names]
    end

    def all?
      @raw_value == '*'
    end

    def numeric_step?
      Regexp.new("^*/#{piped_numeric_values}$").match?(@raw_value) || # "*/12"
          Regexp.new("^#{piped_numeric_values}-#{piped_numeric_values}/#{piped_numeric_values}$").match?(@raw_value) # "2-23/12"
    end

    def numeric_value?
      Regexp.new("^#{piped_numeric_values}$").match? @raw_value # "12"
    end

    def numeric_range?
      # Matches  "(0|1|2|3|4|5|6)-(0|1|2|3|4|5|6)" for day_of_week
      Regexp.new("^#{piped_numeric_values}-#{piped_numeric_values}$").match? @raw_value
    end

    def named_value?
      Regexp.new("^#{piped_named_values}$").match? @raw_value
    end

    def named_range?
      Regexp.new("^#{piped_named_values}-#{piped_named_values}$").match? @raw_value
    end

    def numeric_entry?
      numeric_value? || numeric_range?
    end

    def named_entry?
      named_value? || named_range?
    end
  end
end
