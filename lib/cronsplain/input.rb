require 'cronsplain/field'
require 'pry'

module Cronsplain
  class Input

    def initialize(args)
      @args = args
    end

    # this needs more work
    def parse
      puts 'No arguments provided' unless @args
      offset = 0
      if @args[0] == '-arguments'
        offset = 1
      end

      lines = []
      [:minutes, :hours, :days, :months, :day_of_week].each_with_index do |argument_type, index|
        argument = Cronsplain::Field.new(argument_type, @args[index + offset])
        all_values = argument.valid? ? format_values(argument.values) : ''
        lines << "#{justify(argument_type)} #{all_values}"
      end

      lines << "#{justify('command')} #{@args[5 + offset]}"
    end


    def format_values(values)
      values.map {|v| v.numeric_values}.inject([]) {|acc, val| acc.concat val}.uniq.sort.join(' ')
    end

    def justify(row_name)
      row_name.to_s.gsub(/_/, ' ').ljust(14)
    end

  end
end