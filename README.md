# Cronsplain

Welcome to cronsplain! 

## What it does

Cronsplain parses a cron string and expands each field to show the times at which it will run.

It takes into consideration only the standard cron format with five time fields (minute, hour, day of month, month, and day of week) plus a command.

It does not handle non-standard formats like _@yearly_.


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cronsplain'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install cronsplain
    
## Structure

The bulk of the logic is in the `lib` folder.

```bash
lib
  |
  x-> cronsplain.rb # module entry
  |
  +-> cronsplain
        |
        x-> input.rb # contains the entry point for the input parser (accepts arguments and converts them to fields)
        |
        x-> field.rb # contains the field concept (minutes, days, etc) and knows how to split multiple field values by comma
        |
        x-> field_value.rb #does the heavy lifting. Contains a premapped set of allowed values and tries to parse them to normalized format
```

The printing is done in the `./exe/cronsplain` file as we don't want to tie view to loci (although we do it by returning line for now)
### Considerations

- I did not go for a single Regex for the input params as the resulting regex is too complex. 

- For the sake of readability we predetermine fields and he have a fixed mapping to allowed values.

- In case of `12-3` (for example in months) it expands into: `[12, 1, 2, 3]`

- Field value call themselves in case of range with step: `2-10/2` => FieldValue(2-12).value.step(2) 


## Usage

Call it from terminal with:

    $ ./exe/cronsplain -arguments */15 0 1,15 "*" 1-5 /usr/bin/find

    # OR
    
    $ ./exe/cronsplain */15 0 1,15 "*" 1-5 /usr/bin/find

Or alternatively with:

    $ bundle exec cronsplain */15 0 1,15 "*" 1-5 /usr/bin/find

    $ bundle exec cronsplain -arguments */15 0 1,15 "*" 1-5 /usr/bin/find

**IMPORTANT: use double quotes or quotes around `*` (`"*"`) as bash might expand the star into path.**
 

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

To test use:

    $ bundle exec rspec spec
    
The current tests are at the field level only. (Reason being that i spent some time figuring out why ARGV contains `Gemfle.lock` and other files).

The tests cover the field level functionality and since we don't do any magic in parsing input (positional arguments) the current tests give us confidence that that the parsing functionality is functioning correctly.

All tests are in a single file as they are few (`./spec/cronsplain_spec.rb`)


## Contributing

Bug reports and pull requests are welcome on GitHub at https://butbucket.org/theSKa/misc/cronsplain

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
