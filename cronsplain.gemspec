
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "cronsplain/version"

Gem::Specification.new do |spec|
  spec.name          = "cronsplain"
  spec.version       = Cronsplain::VERSION
  spec.authors       = ['Petru Dimulescu']
  spec.email         = ['petru.dimulescu@theSKa.co']

  spec.summary       = %q{CLI which parses a cron string and expands each field to show the times at which it will run.}
  spec.description   = 'Command line application or script '\
                       'which parses a cron string and expands each field to show the times at which it will run. '\
                       'Considers the standard cron format with five time fields: '\
                       'minute, hour, day of month, month, and day of week) plus a command.'
  spec.homepage      = 'https://cronsplain.org'
  spec.license       = 'MIT'

  # # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # # to allow pushing to a single host or delete this section to allow pushing to any host.
  # if spec.respond_to?(:metadata)
  #   spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  # else
  #   raise "RubyGems 2.0 or newer is required to protect against " \
  #     "public gem pushes."
  # end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.2"
  spec.add_development_dependency "pry", '~> 0.11.3'
  spec.add_development_dependency 'pry-byebug', '~> 3.6.0'

  spec.add_dependency "thor", '~> 0.20'
end
